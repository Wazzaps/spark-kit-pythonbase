#!/usr/bin/env bash

cp spark-kit.svg /usr/share/icons/hicolor/scalable/apps
cp spark-kit-bluetooth-symbolic.svg /usr/share/icons/hicolor/scalable/apps
cp spark-kit-usb-symbolic.svg /usr/share/icons/hicolor/scalable/apps
cp spark-kit-ethernet-symbolic.svg /usr/share/icons/hicolor/scalable/apps
update-icon-caches /usr/share/icons/*
gtk-update-icon-cache
