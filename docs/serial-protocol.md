Spark Kit protocol:
===================

(Master-Device)
(All comm in little-endian)

Identify:
  M>D:
    id {0b00000000}
  D>M:
    num of devices in varint up to 57 bits {0b00000001}
    mfr id {0x00000000000000000000000000000001}
    prod id {0x00000000000000000000000000000001}
    serial number {0x00000000000000000000000000000001}


Trigger: (Hubs propagate triggers immediately/in parallel)
  M>D:
    trig (x=trigger id) {0b01xxxxxx}
      0: capture single sample
      1: start/reset integral
      2-63: reserved

Get:
  M>D:
    get (x=get id) {0b10xxxxxx}
      0: fresh sample
      1: saved sample from trigger
      2: stale sample
      3: avg of last x samples (x=32 by default)
      4: min of last x samples
      5: max of last x samples
      6-63: reserved
  D>M:
    result in varint (word sized) {0b01111111 11111111}

DM:
  M>D:
    dm + dev id in varint up to 57 bits (0b11_0_00000)
    msg length-1 in varint up to 57 bits (0b00000001)
    msg (0b10101010)
  D>M:
    msg length-1 in varint up to 57 bits (0b00000001)
    msg (0b01010101)
    



