import csv
import ctypes


def compress(inp, out):
    for i, row in enumerate(csv.reader(inp)):
        if i == 0:
            for header in row:
                out.write(bytes(header, encoding="utf8"))
                out.write(b'\0')
        else:
            for value in row:
                out.write(ctypes.c_float(float(value)))
