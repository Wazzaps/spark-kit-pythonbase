import select
import threading
import time
import skit_lib.Controllers.PackageLoader as PackLoader
from skit_lib.Controllers.Misc import log
from skit_lib.Models.Proxy import AnyType


def get_device_name(identifier):
    return PackLoader.mapping_table[identifier[:2]][0]


def get_device_type(identifier):
    return PackLoader.mapping_table[identifier[:2]][1].Module


def create_device(identifiers, *args):
    return get_device_type(identifiers)(identifiers, *args)


class DeviceManager:
    def __init__(self, interfaces, lock, data_logger):
        self.interfaces = interfaces
        self.lock = lock
        self.data_logger = data_logger
        self.devs_by_iface = {key: {} for key in interfaces}
        self.fast_mode = False
        self.is_recording = False
        self.should_render = False
        self.should_update = False

        self.thread = threading.Thread(target=self.sample_thread)
        self.thread.start()

    def sample_thread(self):
        while True:
            if PackLoader.exiting:
                log('Global sampler Exited')
                return
            with self.lock:
                curr_time = time.monotonic()
                for iface_name, iface in self.interfaces.items():
                    while not iface['seek_queue'].empty():
                        self.new_seek(iface_name, set(iface['seek_queue'].get()))
                    while not iface['value_queue'].empty():
                        self.new_values(curr_time, iface_name, iface['value_queue'].get())

            # noinspection PyProtectedMember
            select.select(
                [iface['seek_queue']._reader for iface in self.interfaces.values()] +
                [iface['value_queue']._reader for iface in self.interfaces.values()], [], [], 0.5)

    def new_seek(self, iface, seeked):
        removed_devices = self.devs_by_iface[iface].keys() - seeked
        added_devices = seeked - self.devs_by_iface[iface].keys()
        if len(removed_devices) != 0:
            log('Removed:', iface, removed_devices)
            self.should_render = True
        for removed in removed_devices:
            del self.devs_by_iface[iface][removed]

        if len(added_devices) != 0:
            log('Added:', iface, added_devices)
            self.should_render = True
        for identifier in added_devices:
            device = get_device_type(identifier)
            available_proxies = PackLoader.modules['Proxies']
            default_proxies = get_device_type(identifier).DEFAULT_PROXIES
            log_proxies = default_proxies[:default_proxies.index('log')]
            view_proxies = default_proxies[default_proxies.index('log')+1:default_proxies.index('view')]
            my_log_proxies = [available_proxies[prx]['module'].Module() for prx in log_proxies]
            my_view_proxies = [available_proxies[prx]['module'].Module() for prx in view_proxies]

            self.devs_by_iface[iface][identifier] = [None, 'None', None, 'None', None, 'None',
                                                     my_log_proxies, my_view_proxies]

            # Calculate labels
            log_labels = device.VALUE_LABELS
            for proxy in my_log_proxies:
                log_labels = proxy.mutate_labels(log_labels)

            view_labels = device.VALUE_LABELS
            for proxy in my_view_proxies:
                view_labels = proxy.mutate_labels(view_labels)

            self.data_logger.update_labels(
                identifier,
                log_labels,
                view_labels
            )

    def new_values(self, curr_time, iface, values):
        for value in values:
            identifier = value[0][:3]
            device = get_device_type(identifier)
            raw_value = value[1:7]
            self.devs_by_iface[iface][identifier][0:6] = raw_value

            log_proxies, view_proxies = self.devs_by_iface[iface][identifier][6:8]
            out_type = device.OUTPUT_TYPE

            log_value = raw_value
            for proxy in log_proxies:
                if (proxy.INPUT_TYPE == AnyType or
                        out_type == AnyType or
                        proxy.INPUT_TYPE == out_type):
                    log_value = proxy.next(log_value)
                    out_type = proxy.OUTPUT_TYPE
                else:
                    log('Type mismatch! {} != {}'.format(proxy.INPUT_TYPE, out_type), log_type='WARN')

            view_value = log_value
            for proxy in view_proxies:
                if proxy.INPUT_TYPE == out_type:
                    view_value = proxy.next(view_value)
                    out_type = proxy.OUTPUT_TYPE
                else:
                    log('Type mismatch! {} != {}'.format(proxy.INPUT_TYPE, out_type), log_type='WARN')

            self.data_logger.add_point(curr_time, identifier, log_value, view_value)
        self.should_update = True

    def enable_fast_mode(self):
        self.fast_mode = True
        for iface_name, iface in self.interfaces.items():
            iface['control_queue'].put('fast')

    def disable_fast_mode(self):
        self.fast_mode = False
        for iface_name, iface in self.interfaces.items():
            iface['control_queue'].put('slow')

    def reset_integrals(self):
        for iface_name, iface in self.interfaces.items():
            iface['control_queue'].put('reset integrals')
