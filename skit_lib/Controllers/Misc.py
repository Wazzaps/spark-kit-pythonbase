VERSION = 0, 1
CONFIG_VER = 0


def log(*args, log_type='INFO', **kwargs):
    print('[{}]'.format(log_type), *args, **kwargs)
