import importlib
import multiprocessing
import glob
import time
from os import path
import platform
from skit_lib.Controllers.Misc import log

modules = {}
mapping_table = {}

exiting = False


def handle_devices(mod):
    global mapping_table
    log('Loading {}'.format(mod['modpath']))
    mapping_table = {
        **mapping_table,
        **{ids: (device_name, mod['module']) for ids, device_name in mod['module'].device_types.items()}
    }


def handle_interfaces(mod):
    log('Loading {}'.format(mod['modpath']))
    mod['seek_queue'] = multiprocessing.Queue()
    mod['value_queue'] = multiprocessing.Queue()
    mod['control_queue'] = multiprocessing.Queue()
    mod['devices'] = []
    mod['process'] = mod['module'].Module(mod['seek_queue'],
                                          mod['value_queue'],
                                          mod['control_queue'],
                                          mod['module'].SeekThread)
    mod['process'].start()


def handle_proxies(mod):
    log('Loading {}'.format(mod['modpath']))
    pass


def handle_gtk_view(_mod):
    # log(mod['module'].Module)
    pass


def handle_views(mod):
    view_engine = mod['iface_type'].split('_')[1]
    if view_engine == 'GTK':
        handle_gtk_view(mod)
    else:
        log('Unrecognized view engine "{}" when loading {}'.format(view_engine, mod['modpath']), log_type='WARN')
        return
    log('Loading {}'.format(mod['modpath']))


module_type_handlers = {
    'Devices': handle_devices,
    'Interfaces': handle_interfaces,
    'Proxies': handle_proxies,
    'Views': handle_views
}


def last_index_of(arr, elem):
    return len(arr) - 1 - arr[::-1].index(elem)


def reject_underscore(elem):
    return not path.basename(elem[:-1]).startswith('_')


def init():
    # Filter underscores
    packages = filter(reject_underscore, glob.glob("skit_lib/Packages/*/"))

    for package in packages:
        log('Loading package {}'.format(path.basename(package[:-1])))

        module_types = glob.glob(path.join(package, '*_All')) + glob.glob(path.join(package, '*_' + platform.system()))
        for module_type in module_types:
            # Select just the name
            processed_module_type = path.basename(module_type)
            # Remove the platform
            processed_module_type = processed_module_type[:processed_module_type.index('_')]
            filtered_modules = filter(reject_underscore, glob.glob(path.join(module_type, '*/')))
            modules.setdefault(processed_module_type, {})
            for module_path in filtered_modules:
                module_name = path.basename(module_path[:-1])
                dotted_path = '.'.join(module_path[:-1].split('/'))
                dotted_name = '.'.join(module_path[:-1].split('/')[2:])
                module = importlib.import_module(dotted_path)
                modules[processed_module_type][dotted_name] = {
                    'modpath': dotted_name,
                    'fspath': module_path,
                    'name': module_name,
                    'iface_type': path.basename(module_type),
                    'module': module
                }

                if processed_module_type in module_type_handlers:
                    module_type_handlers[processed_module_type](modules[processed_module_type][dotted_name])
                else:
                    log('Unknown module type "{}" when loading "{}"'.format(processed_module_type, module_type),
                        log_type='WARN')


def de_init():
    global exiting

    exiting = True
    for iface in modules['Interfaces'].values():
        iface['control_queue'].put('exit')

    for iface in modules['Interfaces'].values():
        for queue_name in ['control_queue', 'seek_queue', 'value_queue']:
            iface[queue_name].close()

    pool = list(modules['Interfaces'].values())
    while pool:
        for i, iface in enumerate(pool):
            if not iface['process'].is_alive():
                log(iface['name'], 'Exited')
                del pool[i]
        time.sleep(0.01)
