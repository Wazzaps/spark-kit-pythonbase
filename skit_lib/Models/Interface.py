import threading
import time
import multiprocessing
from collections import OrderedDict


class Interface(multiprocessing.Process):
    def __init__(self, seek_queue, value_queue, control_queue, seek_thread, *args, **kwargs):
        super().__init__()
        self.seek_queue = seek_queue
        self.value_queue = value_queue
        self.control_queue = control_queue
        self.fast_mode = False
        self.exiting = [False]

        self.devices = OrderedDict()
        self.seek_thread = seek_thread(self.seek_queue, self.exiting, self.devices, *args, **kwargs)

    def process_control_queue(self):
        if not self.control_queue.empty():
            cmd = self.control_queue.get()
            if cmd == 'exit':
                self.exiting[0] = True
                self.seek_thread.join()
                return False
            elif cmd == 'fast':
                self.fast_mode = True
            elif cmd == 'slow':
                self.fast_mode = False
            elif cmd == 'reset integrals':
                for _, device in self.devices.items():
                    device.reset_integral()
        return True

    # noinspection PyMethodMayBeStatic
    def get_interface(self):
        return True

    def run(self):
        self.seek_thread.start()

        while True:
            self.devices.clear()
            while True:
                # noinspection PyBroadException
                try:
                    if self.get_interface():
                        break
                except Exception:
                    pass

                time.sleep(1)
                if not self.process_control_queue():
                    return

            while True:
                if not self.process_control_queue():
                    return

                if not self.fast_mode:
                    time.sleep(0.05)

                if len(self.devices) == 0:
                    time.sleep(0.5)
                    continue

                # noinspection PyBroadException
                try:
                    if not self.get_values():
                        break
                except Exception:
                    break

    # noinspection PyMethodMayBeStatic
    def get_values(self):
        return True


class SeekThread(threading.Thread):
    def __init__(self, seek_queue, exiting, devices):
        super().__init__()
        self.seek_queue = seek_queue
        self.exiting = exiting
        self.devices = devices

    def run(self):
        while True:
            if self.exiting[0]:
                self.uninit()
                return
            self.get_devices()
            time.sleep(1)

    def get_devices(self):
        pass

    def uninit(self):
        pass
