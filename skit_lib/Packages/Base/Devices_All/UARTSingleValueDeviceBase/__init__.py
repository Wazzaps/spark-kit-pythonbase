import skit_lib.Models.Device

device_types = {}


class Module(skit_lib.Models.Device.Device):
    # noinspection PyMethodMayBeStatic
    def uart_parse_amount(self):
        return 2

    # noinspection PyMethodMayBeStatic
    def uart_post_parse(self, data):
        return int.from_bytes(data, byteorder='little')

    # noinspection PyMethodMayBeStatic
    def uart_custom_parse(self, _ser):
        return 0
