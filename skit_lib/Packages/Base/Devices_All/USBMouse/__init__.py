import skit_lib.Models.Device

device_types = {
    (2, 1): ('PC Generic', 'USB Mouse')
}


class Module(skit_lib.Models.Device.Device):
    VALUE_LABELS = (('ddx', 'ddy'), ('dx', 'dy', 'left', 'middle', 'right'), ('x', 'y'))
    OUTPUT_TYPE = ((float, float), str, (float, float, int, int, int), str, (float, float))
    DEFAULT_PROXIES = ['log', 'Base.Proxies_All.ExtractMouseCoords', 'view']
    DEFAULT_VIEW = 'Base.Views_GTK_All.Simple2DPlot'
    DEFAULT_MINIVIEW = 'Base.MiniViews_GTK_All.SimpleValue'

    def __init__(self, identifier, handle):
        super().__init__(identifier)
        self.set_value_handlers = []
        self.handle = handle

        self.pretty_derivative = 'None'
        self.last_value = (0.0, 0.0)
        self.derivative = (0.0, 0.0)

        self.pretty_value = 'None'
        self.value = (0.0, 0.0, 0, 0, 0)

        self.pretty_integral = 'None'
        self.integral = (0.0, 0.0)

    def set_value(self, value):
        self.last_value = self.value
        self.derivative = (value[0] - self.last_value[0], value[1] - self.last_value[1])
        self.pretty_derivative = '({}, {})'.format(*self.derivative)

        self.value = value
        self.pretty_value = '{}dx {}dy {}l{}m{}r'.format(*self.value)

        self.integral = (self.integral[0] + value[0], self.integral[1] + value[1])
        self.pretty_integral = '({}, {})'.format(*self.integral)

        for handler in self.set_value_handlers:
            handler(self)

    def reset_integral(self):
        self.integral = (0, 0)
