import os
import time
import select

import skit_lib.Models.Interface
import skit_lib.Controllers.DeviceManager as DeviceManager


class Module(skit_lib.Models.Interface.Interface):
    interface_pretty_name = 'USB Mouse'
    interface_icon = 'spark-kit-usb-symbolic'

    def get_interface(self):
        return len([f for f in os.listdir("/dev/input/") if f.startswith("mouse")]) > 0

    def get_values(self):
        if len(self.devices) == 0:
            return False

        handle_to_mouse = {mouse.handle: mouse for _, mouse in self.devices.items()}

        try:
            readable_mice, _, _ = select.select([mouse.handle for _, mouse in self.devices.items()], [], [], 0.1)
            for handle in readable_mice:
                data = handle.read(3)
                left = data[0] & 0x1
                right = (data[0] & 0x2) // 0x2
                middle = (data[0] & 0x4) // 0x4

                unsigned = data[1]
                x = unsigned - 256 if unsigned > 127 else unsigned

                unsigned = data[2]
                y = unsigned - 256 if unsigned > 127 else unsigned

                handle_to_mouse[handle].set_value((x, y, left, middle, right))

            simplified_devices = [dev.simplify() for _, dev in self.devices.items()]
            self.value_queue.put(simplified_devices)
        except (ValueError, OSError):
            pass  # Mouse disconnected

        return True


class SeekThread(skit_lib.Models.Interface.SeekThread):
    def get_devices(self):
        mice = [f for f in os.listdir("/dev/input/") if f.startswith("mouse")]
        expected_mice = ['mouse{}'.format(identifier[2]) for identifier in self.devices]
        if mice != expected_mice:
            for _, mouse in self.devices.items():
                mouse.handle.close()
            self.devices.clear()
            seek_output = []
            for mouse_name in mice:
                mouse_id = int(mouse_name[5:])
                # FIXME: Permission error
                handle = open("/dev/input/" + mouse_name, 'rb')
                identifier = (2, 1, mouse_id)

                self.devices[identifier] = DeviceManager.create_device(identifier, handle)
                seek_output.append(identifier)
            self.seek_queue.put(seek_output)

    def uninit(self):
        for _, mouse in self.devices.items():
            mouse.handle.close()
