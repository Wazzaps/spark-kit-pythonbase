import skit_lib.Models.Proxy


class Module(skit_lib.Models.Proxy.Proxy):
    INPUT_TYPE = float
    OUTPUT_TYPE = float

    def next(self, val):
        return val + 2.5
