import skit_lib.Models.Proxy


class Module(skit_lib.Models.Proxy.Proxy):
    INPUT_TYPE = (float, str, float, str, float, str)
    OUTPUT_TYPE = float

    def next(self, val):
        return val[2]

    def mutate_labels(self, labels):
        return labels[1]
